﻿using System;
using System.IO;
using System.Data;
using System.ComponentModel;

namespace Program
{
    internal class Program
    {
        byte[]? bitmapTile;
        List<byte> TMap = [];
        int paddingCount = 1;
        int paddingFactor = 0;
        static void Main(string[] args)
        {
            Program p = new Program();
            p.Start();
        }
        void Start()
        {
            Console.Write("WorldName: ");
            string worldName = Console.ReadLine();
            Directory.CreateDirectory("Worlds/" + worldName);
            Directory.CreateDirectory("Worlds/" + worldName + "/Towns");
            Directory.CreateDirectory("Worlds/" + worldName + "/Dungeons");
            Directory.CreateDirectory("Worlds/" + worldName + "/Temples");
            bool running = true;
            string? input;
            while (running == true)
            {
                Console.WriteLine("What is the file name?");
                input = Console.ReadLine();
                try
                {
                    bitmapTile = File.ReadAllBytes("TestImages/" + input + ".bmp");
                    int counter = 54;
                    int[] factorAndPadding = PatternIncrament(bitmapTile);
                    paddingFactor = factorAndPadding[0];
                    paddingCount = factorAndPadding[1];
                    //Console.WriteLine($"F{paddingFactor} C{paddingCount}");
                    Console.ReadKey();
                    AddHeader();

                    while (counter <= bitmapTile.Length)
                    {
                        if (counter + 3 + paddingCount > bitmapTile.Length)
                        {
                            Console.WriteLine("Done!!!");
                            try
                            {
                                WriteMapToFile(TMap, worldName);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }
                            Console.WriteLine($"Base map saved at Worlds/{worldName}/map.bm");
                            counter = bitmapTile.Length + 3;//really rub it in
                            break;
                        }
                        if (counter == paddingFactor + 54)
                        {
                            paddingFactor += factorAndPadding[0] + paddingCount;
                            counter += paddingCount;
                        }
                        if (bitmapTile[counter].Equals(111) && bitmapTile[counter + 1].Equals(20) && bitmapTile[counter + 2].Equals(0))
                        {
                            TMap.Add(0x01);//OCEAN!!!
                            //Console.WriteLine("Ocean");
                        }
                        else if (bitmapTile[counter] == 123 && bitmapTile[counter + 1] == 218 && bitmapTile[counter + 2] == 255)
                        {
                            TMap.Add(0x02);//Flat grasslands
                            //Console.WriteLine("GrassFlat");
                        }
                        else if (bitmapTile[counter] == 0x94 && bitmapTile[counter + 1] == 0xFD && bitmapTile[counter + 2] == 0xFF)
                        {
                            TMap.Add(0x03);//Sand beach
                            //Console.WriteLine("SandBeach");
                        }
                        else if (bitmapTile[counter] == 0xCA && bitmapTile[counter + 1] == 0x89 && bitmapTile[counter + 2] == 0x00)
                        {
                            TMap.Add(0x04);//River
                            //Console.WriteLine("River");
                        }
                        else if (bitmapTile[counter] == 0xB6 && bitmapTile[counter + 1] == 0x7F && bitmapTile[counter + 2] == 0xFF)
                        {
                            TMap.Add(0x05);//Dirt Path
                            //Console.WriteLine("DirtPath");
                        }
                        else if (bitmapTile[counter] == 0xDC && bitmapTile[counter + 1] == 0x00 && bitmapTile[counter + 2] == 0xFF)
                        {
                            TMap.Add(0x06);//Stone Road
                            //Console.WriteLine("DirtPath");
                        }
                        else if (bitmapTile[counter] == 0x6E && bitmapTile[counter + 1] == 0x00 && bitmapTile[counter + 2] == 0xFF)
                        {
                            TMap.Add(7);//Wood Path
                            //Console.WriteLine("SandBeach");
                        }
                        else if (bitmapTile[counter] == 0x21 && bitmapTile[counter + 1] == 0xFF && bitmapTile[counter + 2] == 0x00)
                        {
                            TMap.Add(0x08);//Farm House
                            //Console.WriteLine("SandBeach");
                        }
                        else if (bitmapTile[counter] == 0x00 && bitmapTile[counter + 1] == 0xFF && bitmapTile[counter + 2] == 0x4C)
                        {
                            TMap.Add(0x09);//TradeHall
                            //Console.WriteLine("SandBeach");
                        }
                        else if (bitmapTile[counter] == 0x00 && bitmapTile[counter + 1] == 0xFF && bitmapTile[counter + 2] == 0xB6)
                        {
                            TMap.Add(10);//Stone Building
                            //Console.WriteLine("SandBeach");
                        }
                        else if (bitmapTile[counter] == 0x67 && bitmapTile[counter + 1] == 0x67 && bitmapTile[counter + 2] == 0x67)
                        {
                            TMap.Add(11);//stone Beach
                            //Console.WriteLine("StoneBeach");
                        }
                        else if (bitmapTile[counter] == 0x73 && bitmapTile[counter + 1] == 0x00 && bitmapTile[counter + 2] == 0x59)
                        {
                            TMap.Add(12);//Mountain
                            //Console.WriteLine("StoneBeach");
                        }
                        else if (bitmapTile[counter] == 0x01 && bitmapTile[counter + 1] == 0x08 && bitmapTile[counter + 2] == 0xC9)
                        {
                            TMap.Add(13);//Woods
                            //Console.WriteLine("StoneBeach");
                        }
                        else if (bitmapTile[counter] == 0x7A && bitmapTile[counter + 1] == 0xA7 && bitmapTile[counter + 2] == 0x2F)
                        {
                            TMap.Add(14);//Swamp
                            //Console.WriteLine("StoneBeach");
                        }
                        else if (bitmapTile[counter] == 0xDC && bitmapTile[counter + 1] == 0x96 && bitmapTile[counter + 2] == 0xFF)
                        {
                            TMap.Add(15);//Highland Grass
                            //Console.WriteLine("StoneBeach");
                        }
                        else if (bitmapTile[counter] == 0x00 && bitmapTile[counter + 1] == 0x76 && bitmapTile[counter + 2] == 0x1D)
                        {
                            TMap.Add(16);//Woods
                            //Console.WriteLine("StoneBeach");
                        }
                        else
                        {
                            //Console.WriteLine(bitmapTile[counter].ToString("x") + " " + bitmapTile[counter + 1].ToString("x") + " " + bitmapTile[counter + 2].ToString("x"));
                            //Console.WriteLine(counter);
                            //Console.WriteLine("Colour not in pallette filling from left");
                            TMap.Add(TMap[^1]);
                            //Console.WriteLine((paddingFactor * paddingTimes) + 54 + (paddingCount * (paddingTimes - 1) - 1));
                        }
                        counter += 3;
                    }
                }
                catch
                {
                    Console.WriteLine("Something went wrong! Did you spell everything correctly?");
                }
                Console.WriteLine("(C)ustom town placement or (R)andom town placement?");
                input = Console.ReadLine();
                if (input.Contains('C', StringComparison.OrdinalIgnoreCase)) // IMPLEMENT LATER IDIOT
                {

                }
                else //should be if else but i lazy do it later :P
                {
                    Console.WriteLine("Using random town placement");
                    //File.WriteAllBytes("Worlds/" + worldName + "/Towns/TownMap.bm", TownPlacement(bitmapTile)); !!!!!!! uncomment this when done testing
                }
            }

        }
        static int[] PatternIncrament(byte[] bmp)
        {
            int placement = 53;
            int incra = 1;
            int jump = 3;
            int amount = 1;
            bool tracking = true;
            bool amountAcc = false;
            while (tracking)
            {
                if ((placement + jump + amount) >= bmp.Length)//reached end of bmp with no issues
                {
                    break;
                }
                else if (bmp[placement + jump + amount] == 0x00)
                {
                    placement += amount + jump;
                    incra++;
                }
                else
                {

                    amountAcc = false;
                    amount = 1;
                    jump += 3;
                    if (jump % 4 == 0)//numbers like 12 have both 3 and 4 as factors
                    {
                        jump += 3;
                    }
                    while (!amountAcc)
                    {
                        if ((jump + amount) % 4 == 0)
                        {
                            amountAcc = true;
                        }
                        else
                        {
                            amount++;
                        }
                        if (amount > 3)//shouldn't happen!!!!
                        {
                            Console.WriteLine($"PANIC!!! amount is {amount}");
                        }
                    }
                    placement = 53;
                    if (jump > bmp.Length / 2)
                    {
                        return [0, 0];//means there is no padding bc there is no pattern!!
                    }
                }
            }
            return [jump, amount];
        }
        static byte[] TownPlacement(byte[] bmp)
        {
            List<byte> townMap = [];
            bool drawingTerritories = true;
            while (drawingTerritories)//heheheh
            {

            }
            return [1];
        }
        void AddHeader()
        {
            for (int i = 1; i <= 8; i++)//file identification, i'll work it out later when i name the game :P
            {
                TMap.Add(0);
            }
            int totalTileWidth = (paddingFactor / 3) + 1;//might not actually work the patternincrament function is kinda inconsistant idk lol
            byte[] widthBytes = IntToByteArray(totalTileWidth);
            Console.WriteLine($"{totalTileWidth} pixels accross {widthBytes[0]} and {widthBytes[1]}");
            int widthCounter = 0;
            bool addCounter = true;
            for (int i = 1; i <= 4; i++)
            {
                if (addCounter == true)
                {
                    TMap.Add(widthBytes[widthCounter]);
                    widthCounter++;
                }
                else
                {
                    TMap.Add(0);
                }
                if (widthCounter > widthBytes.Length - 1)
                {
                    addCounter = false;
                }
            }
            Console.WriteLine(TMap.Count);
        }
        static void WriteMapToFile(List<byte> map, string mapName)//writes the map from the list to a file
        {
            byte[] mapArray = map.ToArray();
            File.WriteAllBytes("Worlds/" + mapName + "/map.bm", mapArray);
            /*
            List<byte> compressedMap = [];
            for (int i = 0; i <= 11; i++)
            {
                compressedMap.Add(map[i]);
                Console.WriteLine(map[i]);
            }
            int cullPosition = 11;//needs to skip the file header!!!
            int steps = 1;
            byte? lastByte = null;
            bool isCulling = true;
            bool isFirstPos = true;
            while (isCulling)//run-length encoding it so isnt such a big stupid file :P NEEDS TO BE REWRITTEN!!!!
            {
                if (map[cullPosition] != lastByte)
                {
                    if (!isFirstPos)
                    {
                        
                        Console.WriteLine(steps);
                        foreach (byte stepDigit in IntToByteArray(steps))
                        {
                            Console.WriteLine(stepDigit);
                            compressedMap.Add(stepDigit);
                        }
                        //Console.ReadLine();
                        compressedMap.Add(0x00);
                        compressedMap.Add(map[cullPosition]);
                        steps = 1;
                        lastByte = map[cullPosition];
                    }
                    else
                    {
                        compressedMap.Add(map[cullPosition]);
                        lastByte = map[cullPosition];
                    }
                    isFirstPos = false;
                }
                else
                {
                    steps++;
                }

                cullPosition++;
                if (cullPosition > map.Count - 1)
                {
                    foreach (byte stepDigit in IntToByteArray(steps))
                    {
                        compressedMap.Add(stepDigit);
                    }
                    compressedMap.Add(0x00);
                    break;
                }
            }
            byte[] mapArray = compressedMap.ToArray();
            File.WriteAllBytes("Worlds/" + mapName + "/map.bm", mapArray);
            */
        } //NO LONGER USING THE COMPRESSION ALG!!!          ||  technically if a series of bytes is less than 3 bytes long then the compression is actually worse bc the way i did it is dumb lol
        static byte[] IntToByteArray(int input)//converts an int to byte array without unused memory might be the dumbest thing i've programmed for this lol
        {
            List<byte> backToBytes = [];
            List<char> theChars = [];
            string inHex = input.ToString("X");
            int doubling = 0;
            foreach (char i in inHex)
            {
                theChars.Add(i);
                doubling++;
                if (doubling == 2)
                {
                    string soleByte = string.Join("",theChars);
                    backToBytes.Add(byte.Parse(soleByte, System.Globalization.NumberStyles.HexNumber));
                    doubling = 0;
                    theChars.Clear();
                }
            }
            return backToBytes.ToArray();
        }
    }
}